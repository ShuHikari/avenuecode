# Avenue Code Exam #

This repository was intended to create a stateless API for Avenue Code Examination.
It was built in Django and Django Rest Framework 

### Setup ###

* Linux Enviroinment (Preferably Ubuntu)
* Python Version 2.7
* Database Sqlite (django native)
* virtualenv
* pip package manager

* Enviroinment instructions
From within the console command, create the virtualenv:

```mkvirtualenv avenuecode```

Get into the virtualenv and the project folder and install the requirements:

```pip install -r requirements.txt```

* How to run tests
```python manage.py test```

* Run the server
```python manage.py runserver```


Then access from your browser:
http://localhost:8000/products - To view and interact with Product Entity
http://localhost:8000/images - To view and interact with Image Entity


### Overall Notes ###

The content itself wasn't full deilvered cause a lack of time, with more time I certainly will be able to fully implement this kind of API with more features than requested by the exams.
Furthermore, I'll dedicate some more time this weekend and try to deliver something more elaborated to next monday, even if the time is over.

Thanks in advance for the comprehension and support
Best Regards