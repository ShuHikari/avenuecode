# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Image(models.Model):
    '''This class represent the Image model entity'''
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=20, default='thumbnail')

    def __unicode__(self):
        '''Return a human readable representation of the model instance'''
        return self.name


class Product(models.Model):
    '''This class represent the Product model entity'''
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200, null=True)
    image = models.ForeignKey(Image, on_delete=models.CASCADE, null=True)
    parent_product = models.ForeignKey('self', null=True)

    def __unicode__(self):
        '''Return a human readable representation of the model instance'''
        return self.name
