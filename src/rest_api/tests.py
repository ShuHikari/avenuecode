# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from .models import Product, Image
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django.core.urlresolvers import reverse


class ModelTestCase(TestCase):
    '''This class defines the tests for the Models'''

    def setUp(self):
        '''Define the variable tests and the test client'''
        self.image_data = {
            'name': 'first image',
        }
        self.orphan_data = {
            'name': 'Orphan product',
        }
        self.product_data = {
            'name': 'Complete product'
        }

    def test_product_on_single_create(self):
        '''Test if model product can create an orphan object'''
        first_count = Product.objects.count()
        Product.objects.create(**self.orphan_data)
        count = Product.objects.count()
        self.assertNotEqual(first_count, count)

    def test_image_on_create(self):
        '''Test if model image can create an object'''
        first_count = Image.objects.count()
        Image.objects.create(**self.image_data)
        count = Image.objects.count()
        self.assertNotEqual(first_count, count)

    def test_product_on_create_full(self):
        '''Test if the product model can create a complete product'''
        first_count = Product.objects.count()
        Product.objects.create(**self.orphan_data)
        Image.objects.create(**self.image_data)
        self.product_data.update({
            'image': Image.objects.all().last(),
            'parent_product': Product.objects.all().last()
        })
        count = Product.objects.count()
        self.assertNotEqual(first_count, count)


class ViewTestCase(APITestCase):
    '''Test client for rest_api views'''

    def setUp(self):
        '''Define test variables'''
        self.client = APIClient()
        Product.objects.create(name='Test Product')

    def test_api_can_create_product(self):
        '''Test the api for creating a product'''
        self.product_data = {'name': 'Nameless product'}
        self.response = self.client.post(
            reverse('create'),
            self.product_data,
            format='json')
        self.assertEquals(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_product(self):
        '''Test the api can get a given product'''
        product = Product.objects.get()
        response = self.client.get(
            reverse('details', kwargs={'pk': product.id}), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, product)

    def test_api_can_update_product(self):
        '''Test the api can update a given product'''
        product = Product.objects.get()
        edit_product = {'name': 'Renewed name'}
        res = self.client.put(
            reverse('details', kwargs={'pk': product.id}),
            edit_product,
            format='json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_product(self):
        '''Test the api can delete product'''
        product = Product.objects.get()
        response = self.client.delete(
            reverse('details', kwargs={'pk': product.id}),
            format='json',
            follow=True)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
