from rest_framework import serializers
from .models import Product, Image


class product_serializer(serializers.ModelSerializer):
    '''Serializer to map the Product into a JSON format'''

    class Meta:
        '''Meta class to map the model fields'''
        model = Product
        fields = ('id', 'name', 'description', 'image', 'parent_product')


class image_serializer(serializers.ModelSerializer):
    '''Serializer to map Image into a JSON format'''

    class Meta:
        '''Meta Class to map the model fields'''
        model = Image
        fields = ('id', 'name', 'type')
