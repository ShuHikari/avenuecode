from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateImageView, CreateProductView, DetailsView

urlpatterns = [
    url(r'^images/$', CreateImageView.as_view(), name='create'),
    url(r'^products/$', CreateProductView.as_view(), name='create'),
    url(r'^products/(?P<pk>[0-9]+)/$',
        DetailsView.as_view(), name='details')
]

urlpatterns = format_suffix_patterns(urlpatterns)
