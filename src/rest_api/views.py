# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import generics
from .serializers import product_serializer, image_serializer
from .models import Product, Image


class CreateProductView(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = product_serializer

    def perform_create(self, serializer):
        '''Save the POST data when creating a Product'''
        serializer.save()


class CreateImageView(generics.ListAPIView):
    queryset = Image.objects.all()
    serializer_class = image_serializer

    def perform_create(self, serializer):
        '''Save the POST data when creating a Product'''
        serializer.save()


class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    '''Handles the http GET, PUT and DELETE requests'''
    queryset = Product.objects.all()
    serializer_class = product_serializer
